--******INDICES******--

CREATE INDEX cont_reg_id_idx
ON COUNTRIES(region_id); --CREAMOS UN INDEX

CREATE INDEX emps_name_idx
ON employees (first_name,last_name); --INDEX COMPUESTO

CREATE INDEX upper_last_name_idx 
ON employees (UPPER(last_name)); --INDICE SOBRE UNA FUNCION EN UNA COLUMNA

SELECT * FROM employees WHERE UPPER(last_name) = 'KING'; --USAMOS EL INDEX ANTERIOR

SELECT * FROM EMPLOYEES
WHERE UPPER (last_name) IS NOT NULL
ORDER BY UPPER(last_name); --USAMOS EL INDEX TANTO EN EL WHERE COMO EN EL ORDER BY 

CREATE INDEX emp_hire_year_idx
ON employees (TO_CHAR(hire_date,'yyyy'));

SELECT first_name, hire_date
FROM employees
WHERE TO_CHAR(hire_date,'yyyy')='2004';

DROP INDEX emp_hire_year_idx; --PARA BORRAR INDEX

--******SYNONYMS******--

CREATE  SYNONYM amy_emps
FOR employees;

DROP SYNONYM amy_emps;
--creamos una tabla con datos tipo interval to second
create table times ( t1 interval day(0) to second, t2 interval day(0) to second);

--insertamos registros, atenci�n en el formato para ingresar datos
insert into times values ( interval '0 09:30:00' day(0) to second, interval '0 12:30:00' day(0) to second);
insert into times values ( interval '0 10:30:00' day(0) to second, interval '0 13:45:00' day(0) to second);
insert into times values ( interval '0 11:30:00' day(0) to second, interval '0 12:05:01' day(0) to second);

/*Tambi�n se puede insertar as�*/
insert into times values ('0 11:32:00', '0 12:10:01');

select * from times;


--selecciona los registros cuya campo t1 estre ente el intervalo dado
SELECT * FROM times
where  t1 between interval '0 10:00:00' day to second and interval '0 11:00:00' day to second ;
/*Es lo mismo que esto: */
SELECT * FROM times
where  t1 between '0 10:00:00' and '0 11:00:00';

--selecciona la hora, el minuto y el segundo de cada registro
SELECT extract(hour from t1) hr, extract(minute from t1) mn, extract(second from t1) sec
FROM times; 

--selecciona cuando la hora es mayor a las 10:30:00
SELECT * FROM times
where  t1 > '0 10:30:00';

--selecciona el m�ximo de horas de la tabla
Select MAX(t1) FROM times;


--******TIPOS DE DATOS DEFINIDOS POR EL USUARIO******--

--TYPE AS OBJECT--

CREATE TYPE ADDRESS AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
); --CREAMOS UN TIPO OBJECT LLAMADO ADDRESS


CREATE TYPE PERSON AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    ADDRESS
); --CREAMOS UN TIPO OBJETO PERSON, QUE CONTIENE EL OBJECT ADDRESS


--CREAMOS UNA TABLA COMO TIPO PERSON
CREATE TABLE persons OF PERSON;


--CREAMOS UNA TABLA RELACIONADA CON DOS REF'S A OBJETOS TIPO PERSON
CREATE TABLE  employees
( 
  empnumber            INTEGER PRIMARY KEY,
  person_data     REF  PERSON,
  manager         REF  PERSON, --EL REF SIGNIFICA QUE YA DEBE EXISTIR LA PERSONA
  office_addr          ADDRESS,
  salary               NUMBER
);

--INSERTAMOS DATOS EN LA TABLA PERSONS--
INSERT INTO persons VALUES (
            PERSON('Wolfgang Amadeus Mozart', 123456,
               ADDRESS('Am Berg 100', 'Salzburg', 'AT','10424')));

INSERT INTO persons VALUES (
            PERSON('Ludwig van Beethoven', 234567,
               ADDRESS('Rheinallee', 'Bonn', 'DE', '69234')));
--SE DEBEN INCLUIR LOS TIPOS Y ENTRE PARENTESIS SUS ATRIBUTOS :D

--AHORA INSERTAMOS UN REGISTRO EN LA TABLA EMPLEADOS (SIN LOS REF�S)
INSERT INTO employees (empnumber, office_addr, salary) VALUES (
            1001,
            ADDRESS('500 Oracle Parkway', 'Redwood Shores', 'CA', '94065'),
            50000);

--ACTUALIZAMOS EL REGISTRO DE EMPLEADOS PARA COLOCAR LOS REF'S
UPDATE employees 
   SET manager =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Wolfgang Amadeus Mozart');

UPDATE employees 
   SET person_data =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven');
       
--CONSULTAMOS DATOS
SELECT * FROM PERSONS; --NO MUESTRA LA ADDRESS

SELECT * FROM EMPLOYEES; --NO MUESTRA LOS DATOS DE OBJETOS






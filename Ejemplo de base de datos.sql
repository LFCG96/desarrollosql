--EJEMPLO DE BASE DE DATOS PARA UN RELOJ CHECADOR

-- CREAR TABLAS 
CREATE TABLE EMPLEADOS (
    RFC VARCHAR2(10) NOT NULL,
    Nombres VARCHAR2(20) NOT NULL,
    Ap_paterno VARCHAR2(20) NOT NULL,
    Ap_materno VARCHAR2(20) NOT NULL,
    H_entrada interval day(0) to second NOT NULL,
    H_salida interval day(0) to second NOT NULL,
    Dias_economicos NUMBER(2) NOT NULL,
    Turno VARCHAR2(4) NOT NULL,
    Fecha_ingreso DATE NOT NULL
);

CREATE TABLE PROFESORES (
    RFC VARCHAR2(10) NOT NULL,
    Grado NUMBER(1) NOT NULL,
    Grupo VARCHAR2(1) NOT NULL,
    Numero_Alumnos NUMBER(2) NOT NULL
);

CREATE TABLE ADMINISTRATIVOS (
    RFC VARCHAR2(10) NOT NULL,
    Puesto VARCHAR2(20) NOT NULL,
    Tel_oficina VARCHAR2(12) NOT NULL
);

CREATE TABLE PERSONAL_EXTRA (
    RFC VARCHAR2(10) NOT NULL,
    Cargo VARCHAR2(13) NOT NULL
);

CREATE TABLE REGISTROEYS (
    RFC VARCHAR2(10) NOT NULL,
    Fecha DATE NOT NULL,
    Hora_Entrada interval day(0) to second NOT NULL,
    Hora_Salida interval day(0) to second NOT NULL
);

CREATE TABLE PERMISOS (
    RFC VARCHAR2(10) NOT NULL,
    Fecha_inicio DATE NOT NULL,
    Fecha_fin DATE NOT NULL,
    Motivo VARCHAR2(20),
    Justificante NUMBER(1) NOT NULL
);

CREATE TABLE DIAFESTIVO (
    Fecha DATE NOT NULL,
    Descripcion VARCHAR2(20) NOT NULL
);

--CONSTRAINTS PK, FK, BOOLEANOS, MASCARAS Y DOMINIOS

ALTER TABLE EMPLEADOS ADD (
    CONSTRAINT empleados_pk PRIMARY KEY(RFC), --CONSTRAINT PARA PRIMARY KEY
    CONSTRAINT check_empleados_rfc CHECK(regexp_like(RFC, '^[A-Z]{4}\d{6}$')), -- FORMATO PARA RFC: 4 CARACTERES Y 6 DIGITOS
    CONSTRAINT check_emp_dias_economicos CHECK(Dias_economicos <= 15), -- NO PUEDE TENER MAS DE 15 DIAS ECONOMICOS
    CONSTRAINT check_empleados_turno CHECK(Turno IN ('MAT', 'VES')) -- VALIDACION DE OPCION MULTIPLE
);

ALTER TABLE PROFESORES ADD (
    CONSTRAINT profesores_pk PRIMARY KEY(RFC),
    CONSTRAINT check_profesores_rfc CHECK(regexp_like(RFC, '^[A-Z]{4}\d{6}$')),
    CONSTRAINT check_profesores_grado CHECK(Grado IN (1,2,3,4,5,6)),
    CONSTRAINT check_profesores_grupo CHECK(Grupo IN ('A', 'B', 'C')),
    CONSTRAINT profesores_fk_column FOREIGN KEY(RFC) REFERENCES EMPLEADOS(RFC) --CONSTRAINT PARA FOREIGN KEY
);

ALTER TABLE ADMINISTRATIVOS ADD (
    CONSTRAINT administrativos_pk PRIMARY KEY(RFC),
    CONSTRAINT check_administrativos_rfc CHECK(regexp_like(RFC, '^[A-Z]{4}\d{6}$')),
    CONSTRAINT check_administrativos_telefono CHECK(regexp_like(Tel_oficina, '^\d{8}\-\d{3}$')),
    CONSTRAINT administrativos_fk_column FOREIGN KEY(RFC) REFERENCES EMPLEADOS(RFC)
);

ALTER TABLE PERSONAL_EXTRA ADD (
    CONSTRAINT personal_extra_pk PRIMARY KEY(RFC),
    CONSTRAINT check_personal_extra_rfc CHECK(regexp_like(RFC, '^[A-Z]{4}\d{6}$')),
    CONSTRAINT check_personal_extra_cargo CHECK(Cargo IN('Vigilancia', 'Jardinerķa', 'Mantenimiento', 'Intendencia')),
    CONSTRAINT personal_extra_fk_column FOREIGN KEY(RFC) REFERENCES EMPLEADOS(RFC)
);

ALTER TABLE REGISTROEYS ADD (
    CONSTRAINT registroeys_pk PRIMARY KEY(RFC, Fecha),
    CONSTRAINT check_registroeys_rfc CHECK(regexp_like(RFC, '^[A-Z]{4}\d{6}$')),
    CONSTRAINT registroeys_fk_column FOREIGN KEY(RFC) REFERENCES EMPLEADOS(RFC)
);

ALTER TABLE PERMISOS ADD (
    CONSTRAINT permisos_pk PRIMARY KEY(RFC, Fecha_inicio, Fecha_fin),
    CONSTRAINT check_permisos_rfc CHECK(regexp_like(RFC, '^[A-Z]{4}\d{6}$')),
    CONSTRAINT check_permisos_justificante CHECK(Justificante IN(0,1)), -- Booleano!!
    CONSTRAINT permisos_fk_column FOREIGN KEY(RFC) REFERENCES EMPLEADOS(RFC)
);

ALTER TABLE DIAFESTIVO ADD CONSTRAINT diafestivo_pk PRIMARY KEY(Fecha);
select * from EMP; -- Consulta simple

select ENAME, JOB from EMP where ENAME='KING'; --Consulta con condici�n para limitar los rows a mostrar

select ENAME AS "Nombre" from EMP; --Alias 

Describe EMP; --Describir una tabla y sus caracteristicas 

Select ENAME || ' ' || JOB AS "Clave" from EMP; --Concatena

Select DISTINCT SAL From EMP; -- Elimina columnas repetidas

select ENAME, JOB from EMP where HIREDATE<'01-01-1982'; --Strings y date van as� 

select ENAME, HIREDATE from EMP where HIREDATE BETWEEN '01-01-1981' AND '01-01-1982';

select * from EMP where HIREDATE IN('17/11/1981');

select * from EMP where ENAME LIKE '_I%';

select * from EMP where MGR IS not NULL;

select * from EMP where MGR IS not NULL AND ENAME LIKE '_I%';

select * from EMP where MGR IS not NULL OR ENAME LIKE '_I%';

select * from EMP where HIREDATE NOT IN('17/11/1981');

select * from EMP order by ENAME DESC;

--Manipulaci�n de caracteres--

select LOWER(ENAME) from  EMP where LOWER(ENAME)='king'; --UPPER mayusculas, INITCAP para la primera mayuscula

select  concat('Hello',' World') from dual;

select substr('Hello World',1,5) from dual;

select length('Hola') from dual;

select ENAME,DEPTNO from EMP where DEPTNO=:enter_dept_id; --Los : sirven para indicar variables

--Number Functions--

--Round lleva el valor y numero de decimales
select ROUND(SAL*.355,1) from EMP;

--Trunc es igual pero sin redondear, solo corta
select TRUNC(SAL*.355,1) from EMP;

--MOD es el modulo o residuo de la divisi�n
select MOD(SAL*.355,2) from EMP;

--DATE FUNCTIONS-----

select sysdate+2 from dual; --Retorna la fecha actual m�s dos d�as
select (sysdate-hiredate)/7 from emp; -- Retorna # de semanas en el periodo dado

select MONTHS_BETWEEN(SYSDATE,hiredate) from emp;
select add_months(sysdate,12) from dual;
select next_day(sysdate,6) from dual;

--conversion----
select to_char(hiredate,'Month dd, YYYY') from emp;
select to_char(hiredate,'fmMonth dd, YYYY') from emp;
select to_char(hiredate,'Month ddth, YYYY') from emp;
select to_char(sysdate, 'hh:mm pm') from dual;

--NULLS-----
select NVL(MGR,000) from emp;--sustituye el null
select NVL2(MGR, MGR*2,1) from emp; -- si 1 es null imprime 3, sino 2
select NULLIF('Hola','Hola') from dual; --si son iguales retorna null

--Expresiones condicionales----

Select ENAME, --ANSI 
CASE DEPTNO 
    WHEN 10 THEN 'CLEAN'
    WHEN 20 THEN 'TI'
    ELSE 'ECO'
END AS "Departamento" 
FROM EMP;

Select ENAME, --ORACLE
DECODE (DEPTNO, 
    10,'CLEAN',
    20,'TI',
    'ECO')
AS "Departamento" 
FROM EMP;

--CROSS y NATURAL JOINS--

Select ENAME, DNAME
From EMP NATURAL JOIN DEPT
Where DEPTNO=10; --Sin indicar de quien es cada columna ni clausula on, solo requiere el mismo campo en ambas talas

Select ENAME, DNAME
From EMP CROSS JOIN DEPT; --Cruza indistintamente todos los registros de las tablas

Select ENAME, DNAME
From EMP e JOIN DEPT d
ON e.DEPTNO = d.DEPTNO
Where ENAME LIKE '_I%'; --Alias y clausula ON

SELECT worker.ENAME || ' works for ' || manager.ENAME AS "Works for"
FROM EMP worker JOIN EMP manager
ON worker.manager_id = manager.employee_id; --Si existieran los campos un Self-join ayudar�a a hacer recursivo el recorrido

-- ORACLE PROPIETARY JOIN (EQUIJOIN)--
Select EMP.ENAME, DEPT.DNAME --Que?
FROM EMP,DEPT                --Donde?
WHERE EMP.DEPTNO = DEPT.DEPTNO; --C�mo? -- para otra restricci�n agregar AND

--PRODUCTO CARTESIANO----
Select EMP.ENAME, DEPT.DNAME
FROM EMP, DEPT;

--NON EQUIJOIN--
Select ENAME, SAL, GRADE_LEVEL, LOWEST_SAL, HIGHEST_SAL
FROM EMP, JOB_GRADES
WHERE SAL BETWEEN LOWEST_SAL AND HIGHEST_SAL; --No existen pero seria para unir dos tablas sin columnas en comun pero comparando SAL de una tabla con dos campos de la otra tabla

--FUNCIONES DE GRUPO --
SELECT MAX(SAL) FROM EMP;
SELECT Min(SAL) FROM EMP;
SELECT AVG(SAL) FROM EMP;
SELECT SUM(SAL) FROM EMP;

SELECT ENAME, (SELECT COUNT(*) FROM EMP ) AS "TOTAL"
FROM EMP;

SELECT COUNT(*) FROM EMP WHERE HIREDATE<'01/01/1982';

SELECT DEPTNO, AVG(SAL) 
FROM EMP 
GROUP BY DEPTNO;

SELECT DEPTNO, MAX(SAL)
FROM EMP
GROUP BY DEPTNO
HAVING COUNT(*)>4; --Solo muestra los grupos que internamente tengan mas de 4 registros

--ROLL UP Para subtotales--
SELECT DEPTNO,JOB, SUM (SAL)
FROM EMP
WHERE DEPTNO>=10
GROUP BY ROLLUP (DEPTNO,JOB);

--CUBE--
SELECT DEPTNO,JOB, SUM (SAL)
FROM EMP
WHERE DEPTNO>=10
GROUP BY CUBE (DEPTNO,JOB); --Muestra total, total por puesto, por departamento y el desglose de cada uno

--GROUPING SETS--
SELECT DEPTNO, JOB, MGR, SUM(SAL)
FROM EMP
GROUP BY GROUPING SETS 
((JOB,MGR),(DEPTNO,JOB),(DEPTNO,MGR));

--GROUPING FUNCTIONS--
SELECT DEPTNO,JOB,SUM(SAL),
        GROUPING(DEPTNO) AS "DEPT SUB TOTAL",
        GROUPING(JOB) AS "JOB SUB TOTAL"
FROM EMP
GROUP BY CUBE (DEPTNO,JOB); --Si es dato computado y 0 si es de la BD

--SET OPERATORS--
CREATE TABLE A (A_ID NUMBER NOT NULL);
CREATE TABLE B (B_ID NUMBER NOT NULL);
Insert into B Values(8);

Select a_id from a
UNION 
Select b_id from b;

Select a_id from a
UNION ALL
Select b_id from b;

Select a_id from a
INTERSECT
Select b_id from b;

Select a_id from a
MINUS
Select b_id from b;

--SUBQUERYS (SINGLE ROW)--
SELECT ENAME, HIREDATE
FROM EMP
WHERE HIREDATE>
    (SELECT HIREDATE FROM EMP WHERE ENAME='KING'); --Es un single row subquery porque solo retorna un registro
    
SELECT ENAME, JOB, DEPTNO
FROM EMP
WHERE DEPTNO=
    (SELECT DEPTNO FROM DEPT WHERE DNAME = 'SALES')
ORDER BY JOB; --Pueden ser de diferentes tablas

SELECT ENAME,SAL FROM EMP WHERE SAL < (SELECT AVG(SAL) FROM EMP); --SE PUEDEN USAR FUNCIONES DE GRUPO

--SUBQUERYS (MULTIPLE ROW)--
SELECT ENAME, HIREDATE FROM EMP
WHERE EXTRACT(YEAR FROM HIREDATE) IN 
(SELECT EXTRACT(YEAR FROM HIREDATE) FROM EMP WHERE DEPTNO=30);
    
SELECT ENAME, HIREDATE FROM EMP
WHERE EXTRACT(YEAR FROM HIREDATE) > ANY
(SELECT EXTRACT(YEAR FROM HIREDATE) FROM EMP WHERE DEPTNO=30);

SELECT ENAME, HIREDATE FROM EMP
WHERE EXTRACT(YEAR FROM HIREDATE) < ALL
(SELECT EXTRACT(YEAR FROM HIREDATE) FROM EMP WHERE DEPTNO=30);

SELECT o.ENAME, o.SAL
FROM EMP o 
WHERE o.SAL > (SELECT AVG(i.SAL) FROM EMP i WHERE i.DEPTNO = o.DEPTNO);--SUBQUERY CORRELACIONADO

SELECT ENAME AS "NOT A MANAGER"
FROM EMP emp
WHERE NOT EXISTS (SELECT * FROM EMP mgr WHERE mgr.MGR = emp.EMPNO);

WITH managers AS (SELECT DISTINCT MGR FROM EMP WHERE MGR IS NOT NULL)
SELECT ENAME AS "NOT A MANAGER" FROM EMP WHERE EMPNO NOT IN (SELECT*FROM managers); --SUBQUERY CON CLAUSUSLA WITH

--EJERCICIOS--

SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME LIKE 'DE%';

SELECT SUBSTR(ENAME,1,1) || ' ' || JOB FROM EMP;

-- COPIAR TABLAS --
CREATE TABLE COPY_EMP
AS (SELECT * FROM EMP);

INSERT INTO DEPT (DEPTNO,DNAME,LOC) VALUES (50,'HOLI','MEXICO');

UPDATE DEPT SET DNAME = 'HOLO XD'
WHERE DEPTNO = 50; --TANTO EL VALOR COMO LA CONDICI�N PUEDEN SER SUBQUERYS Y SE PUEDEN ACTUALIZAR VARIOS VALORES

DELETE FROM DEPT WHERE DEPTNO = 50; --EL WHERE PUEDE TENER UN SUBQUERY

SELECT * FROM DEPT;

--SEQUENCE--
CREATE SEQUENCE seq
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 50000
  NOCACHE
  NOCYCLE;

SELECT SEQUENCE_NAME,MIN_VALUE,MAX_VALUE,INCREMENT_BY FROM USER_SEQUENCES;

--AL INSERTAR EN UNA TABLA SE PUEDE PONER ALGO COMO VALUES(...seq.NEXTVAL...)

--INDEX-------------------

CREATE INDEX deptno_idx ON DEPT(DEPTNO);
--ADENTRO PUEDEN IR FUNCIONES COMO UPPER Y AL USAR EN UN SELECT TAL CUAL APAREZCA EN EL INDEX, ORACLE HAR� LA BUSQUEDA M�S R�PIDA Y EFICIENTE

--USUARIOS--**************************
CREATE USER luis96 IDENTIFIED BY luis96; --CREAR
ALTER USER luis96 IDENTIFIED BY luisfcg96; --MODIFICAR PASS
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, TO luis96; --DAR PERMISOS
GRANT UPDATE (SAL) ON EMP TO luis96; --PERMISO SOBRE UNA COLUMNA EN ESPECIFICO 
GRANT SELECT ON luis96.dept TO PUBLIC; --TODOS LOS USUARIOS PUEDEN VER ESA TABLA

--ROLES--
CREATE ROLE manager; --CREA ROL
GRANT CREATE TABLE, CREATE VIEW TO manager; --ASIGNA PERMISOS
GRANT manager TO luis96; --ASIGNA ROL AL USUARIO

REVOKE SELECT, INSERT ON EMP FROM luis96; --REVOCAR PERMISOS DEL USUARIO

--LINK A OTRA BD
CREATE PUBLIC SYNONYM HQ_EMP
    FOR emp@HQ.ACME.COM;

--EXPRESIONES REGULARES--
SELECT ENAME, JOB FROM EMP WHERE REGEXP_LIKE(ENAME, '^.(I|A).G$');

ALTER TABLE EMP ADD CONSTRAINT ename_chk
CHECK (REGEXP_LIKE(ENAME,'^[A-Z]+'));



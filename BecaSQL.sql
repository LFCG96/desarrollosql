DECLARE --usando records
    TYPE t_emp_rec IS TABLE OF emp%ROWTYPE
        INDEX BY BINARY_INTEGER;
    v_emp_rec_tab t_emp_rec;
BEGIN
    FOR emp_rec IN (SELECT * FROM emp) LOOP
        v_emp_rec_tab(emp_rec.empno) := emp_rec;
        DBMS_OUTPUT.PUT_LINE(v_emp_rec_tab(emp_rec.empno).sal||' hola');
    END LOOP;
END;